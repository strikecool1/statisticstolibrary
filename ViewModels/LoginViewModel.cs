﻿using Core_Library.Commands;
using Core_Library.Service;
using Core_Library.ViewModels.Base;
using System.Windows.Input;

namespace Core_Library.ViewModels
{
    internal class LoginViewModel : BaseViewModel
    {
        public LoginViewModel(NavigationStore navigationStore)
        {
            this.navigationStore = navigationStore;
        }

        #region Переменные

        private readonly NavigationStore navigationStore;
        // Введённый логин
        private string _UserName;
        public string UserName
        {
            get => _UserName;
            set => Set(ref _UserName, value);
        }
        // Введённый Пароль
        private string _Password;
        public string Password
        {
            get => _Password;
            set => Set(ref _Password, value);
        }
        // Сообщение ошибки
        private string _ErrorMessage;
        public string ErrorMessage
        {
            get => _ErrorMessage;
            private set
            {
                Set(ref _ErrorMessage, value);
                OnPropertyChanged(nameof(IsErrorVisible));
            }
        }
        // Свойство отображения уведомления при не валидных данных 
        public bool IsErrorVisible => !string.IsNullOrEmpty(ErrorMessage);

        #endregion

        #region Команды

        // Команда Авторизации
        private ICommand _SignIn;
        // Лямбда Команда 
        public ICommand SignIn => _SignIn ??= new LambdaCommand(OnSignInAsync, CanSignIn);

        #endregion

        #region Логика Команд

        // Доступность
        private bool CanSignIn(object parameter) => !string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(Password);

        // Логика Авторизации
        private void OnSignInAsync(object p)
        {
            if(UserName == "root" && Password == "1297")
            {
                navigationStore.CurrentViewModel = new HomeViewModel(navigationStore);
            }
            else
            {
                ErrorMessage = "Неверный логин или пароль";
            }
        }

        #endregion

        // Освобождаем ресурсы
        public override void Dispose()
        {
            base.Dispose();
        }

    }
}
