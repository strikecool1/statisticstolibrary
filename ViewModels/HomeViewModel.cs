﻿using Core_Library.Commands;
using Core_Library.Models;
using Core_Library.Service;
using Core_Library.ViewModels.Base;
using Core_Library.Views;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace Core_Library.ViewModels
{
    internal class HomeViewModel: BaseViewModel
    {
        public HomeViewModel(NavigationStore navigationStore)
        {
            this.navigationStore = navigationStore;
        }

        #region переменные

        private NavigationStore navigationStore;

        private VisualBoolean _IsLoading;
        public VisualBoolean IsLoading
        {
            get => _IsLoading;
            set => Set(ref _IsLoading, value);

        }

        private int _SummAll;

        public int SummAll
        {
           get => _SummAll;
            set => Set(ref _SummAll, value);
        }

        // Ссылка на SQL FireBird
        private readonly FirebirdHelper helper = new();

        private TheOneSotr _TheOneStud2;

        public TheOneSotr TheOneStud2
        {
            get => _TheOneStud2;
            set => Set(ref _TheOneStud2, value);
        }

        private TheOneSotr _TheOneStud3;

        public TheOneSotr TheOneStud3
        {
            get => _TheOneStud3;
            set => Set(ref _TheOneStud3, value);
        }

        private TheOneSotr _TheOneSotr;

        public TheOneSotr TheOneSotr
        {
            get => _TheOneSotr;
            set => Set(ref _TheOneSotr, value);
        }

        private ObservableCollection<Persons> _Persons;

        public ObservableCollection<Persons> Persons
        {
            get => _Persons;
            set => Set(ref _Persons, value);
        }

        private ObservableCollection<Histrory> _Histrory;

        public ObservableCollection<Histrory> Histrory
        {
            get => _Histrory;
            set => Set(ref _Histrory, value);
        }

        private DateTime _DateBegin = DateTime.Now;

        public DateTime DateBegin
        {
            get => _DateBegin;
            set => Set(ref _DateBegin, value);
        }

        private DateTime _DateEnd = DateTime.Now;

        public DateTime DateEnd
        {
            get => _DateEnd;
            set => Set(ref _DateEnd, value);
        }

        // Вернуть выдачу (посещений)
        private ObservableCollection<VisitPerson> _Get_CountVisit;

        public ObservableCollection<VisitPerson> Get_CountVisit
        {
            get => _Get_CountVisit;
            set => Set(ref _Get_CountVisit, value);
        }

        private ObservableCollection<VisitPerson> _Return_CountVisit;
        // возврат (посещения)
        public ObservableCollection<VisitPerson> Return_CountVisit
        {
            get => _Return_CountVisit;
            set => Set(ref _Return_CountVisit, value);
        }
        #endregion

        #region команды

        private ICommand _GetPerson;
        public ICommand GetPerson => _GetPerson ??= new LambdaCommand(LoadedPersonTable);

        //Открыть модальное окно 
        private ICommand _OpenCategoryWindowCommand;
        public ICommand OpenCategoryWindowCommand => _OpenCategoryWindowCommand ??= new LambdaCommand(OpenCategoryWindow);

        //Открыть модальное окно 
        private ICommand _OpenPlaceWindowCommand;
        public ICommand OpenPlaceWindowCommand => _OpenPlaceWindowCommand ??= new LambdaCommand(OpenPlaceWindow);

        //Открыть модальное окно 
        private ICommand _OpenOneWindowCommand;
        public ICommand OpenOneWindowCommand => _OpenOneWindowCommand ??= new LambdaCommand(OpenTheOneWindow);


        private ICommand _OpenOneStudWindowCommand;
        public ICommand OpenOneStudWindowCommand => _OpenOneStudWindowCommand ??= new LambdaCommand(OpenTheOneStudWindow);

        //Открыть модальное окно 
        private ICommand _OpenVisitWindowCommand;

        public ICommand OpenVisitWindowCommand => _OpenVisitWindowCommand ??= new LambdaCommand(OpenVisitPlaceWindow);

        //Открыть модальное окно 
        private ICommand _OpenHistorytWindowCommand;

        public ICommand OpenHistorytWindowCommand => _OpenHistorytWindowCommand ??= new LambdaCommand(OpenHistoryWindow);

        //Открыть модальное окно 
        private ICommand _OpenTypeWindowCommand;

        public ICommand OpenTypeWindowCommand => _OpenTypeWindowCommand ??= new LambdaCommand(OpenTypeWindow);

        #endregion

        #region логика

        // Вернуть книговыдачу книг
        private async void LoadedPersonTable(object p)
        {
            // Включаем progressbar
            IsLoading = true;
            try
            {
                // Книговыдача (процедура)
                Persons = await helper.GetBooks(DateBegin.ToShortDateString(), DateEnd.ToShortDateString());
                // Сотрудники обслуженные
                TheOneSotr = await helper.GetTheOneSotr(DateBegin.ToShortDateString(), DateEnd.ToShortDateString(), 4);
                // Студенты обслуженные
                TheOneStud2 = await helper.GetTheOneStud(DateBegin.ToShortDateString(), DateEnd.ToShortDateString(), 2);
                // Студенты обслуженные
                TheOneStud3 = await helper.GetTheOneStud(DateBegin.ToShortDateString(), DateEnd.ToShortDateString(), 3);
                // Остальные обслуженные
                Histrory = await helper.GetKOSTIL(DateBegin.ToShortDateString(), DateEnd.ToShortDateString());


                SummAll = Histrory.Sum(x => x.Counts)
                    // Обслуженные у второго отдела
                    + TheOneStud2.Count_Magistatura
                    + TheOneStud2.Count_Dovuz
                    + TheOneStud2.Count_Prepod
                    + TheOneStud2.Count_Aspirant
                    + TheOneStud2.Count_Sotr

                    // Обслуженные у третего отдела
                    + TheOneStud3.Count_Prepod
                    + TheOneStud3.Count_Aspirant
                    + TheOneStud3.Count_Sotr

                    // Обслуженные у сотрудников
                    + TheOneSotr.Count_Dovuz +
                     TheOneSotr.Count_Magistatura +
                     TheOneSotr.Count_Mladg_Special +
                     TheOneSotr.Count_Stud_DO +
                     TheOneSotr.Count_Stud_EK +
                     TheOneSotr.Count_Stud_ZO;
                // Посещение выдача
                Get_CountVisit = await helper.GetVisit(DateBegin.ToShortDateString(), DateEnd.ToShortDateString());
                // Посещение возврат
                Return_CountVisit = await helper.ReturnVisit(DateBegin.ToShortDateString(), DateEnd.ToShortDateString());



                // Выключаем progressbar
                IsLoading = false;
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }
           
        }


        // Открыть модальное окно "Книгавыдачи по категориям"
        private void OpenCategoryWindow(object p)
        {
            // windows.OpenBookCategory();
            BookCategoryViewModel view_model = new(helper, DateBegin, DateEnd);
            BookCategoryView view = new() { DataContext = view_model };
            _ = view.ShowDialog();
        }

        // Открыть модальное окно "Книгавыдачи по месту"
        private void OpenPlaceWindow(object p)
        {
            BoolPlaceViewModel view_model = new(helper, DateBegin, DateEnd);
            BookPlaceView view = new() { DataContext = view_model };
            _ = view.ShowDialog();

        }

        // Открыть модальное окно "Посещение по месту"
        private void OpenVisitPlaceWindow(object p)
        {
            VisitPlaceViewModel view_model = new(helper, DateBegin, DateEnd);
            VisitPlaceView view = new() { DataContext = view_model };
            _ = view.ShowDialog();
        }

        // Открыть модальное окно "По единому"
        private void OpenTheOneWindow(object p)
        {
            TheOneViewModel view_model = new(helper, DateBegin, DateEnd);
            TheOneView view = new() { DataContext = view_model };
            _ = view.ShowDialog();
        }


        private void OpenTheOneStudWindow(object p)
        {
            TheOneStudViewModel view_model = new(helper, DateBegin, DateEnd);
            TheOneStudView view = new() { DataContext = view_model };
            _ = view.ShowDialog();
        }

        private void OpenHistoryWindow(object p)
        {
            HistoryViewModel view_model = new(helper);
            HistoryView view = new() { DataContext = view_model };
            _ = view.ShowDialog();
        }

        private void OpenTypeWindow(object p)
        {
            TypeViewModel view_model = new(helper, DateBegin, DateEnd);
            TypeView view = new() { DataContext = view_model };
            _ = view.ShowDialog();
        }

        #endregion

        // Освобождаем ресурсы
        public override void Dispose()
        {
            base.Dispose();
        }

    }
}
