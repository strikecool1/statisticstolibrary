﻿using Core_Library.Commands;
using Core_Library.Models;
using Core_Library.Service;
using Core_Library.ViewModels.Base;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Core_Library.ViewModels
{
    internal class VisitPlaceViewModel : BaseViewModel
    {
        #region Перменные

        private ObservableCollection<Departments> _Departments;

        private readonly FirebirdHelper helper;

        private DateTime _DateBegin;

        public DateTime DateBegin
        {
            get => _DateBegin;
            set => Set(ref _DateBegin, value);
        }

        private DateTime _DateEnd;

        public DateTime DateEnd
        {
            get => _DateEnd;
            set => Set(ref _DateEnd, value);
        }

       

        public ObservableCollection<Departments> Departments
        {
            get => _Departments;
            set => Set(ref _Departments, value);
        }

        private ICommand _LoadedDep;
        public ICommand LoadedDep => _LoadedDep ??= new LambdaCommand(LoadedDepart);


        private ICommand _SelectedCountPlace;
        public ICommand SelectedCountPlace => _SelectedCountPlace ??= new LambdaCommand(SelectCountVisit, CanLoadedCount);

        // Вывести список только в том случае если выбран абонемент
        private bool CanLoadedCount(object parameter) => SelectedDepartment != null;


        private Departments _SelectedDepartment;
        public Departments SelectedDepartment
        {
            get => _SelectedDepartment;
            set => Set(ref _SelectedDepartment, value);
        }

        private ObservableCollection<VisitPerson> _Get_CountVisit;
        // возврат (посещения)
        public ObservableCollection<VisitPerson> Get_CountVisit
        {
            get => _Get_CountVisit;
            set => Set(ref _Get_CountVisit, value);
        }

        private ObservableCollection<VisitPerson> _Return_CountVisit;
        // возврат (посещения)
        public ObservableCollection<VisitPerson> Return_CountVisit
        {
            get => _Return_CountVisit;
            set => Set(ref _Return_CountVisit, value);
        }
        // Всего
        private int _Count_Visit;

        public int Count_Visit
        {
            get => _Count_Visit;
            set => Set(ref _Count_Visit, value);
        }


        #endregion

        #region Логика

        // Загрузка всех абонементов
        private async void LoadedDepart(object p)
        {
            Departments = await helper.GetAllDepartments();
        }

        private async void SelectCountVisit(object p)
        {
            Get_CountVisit = await helper.VisitPlace_Get(DateBegin.ToShortDateString() , DateEnd.ToShortDateString() , SelectedDepartment.Id);

            Return_CountVisit = await helper.VisitPlace_Return(DateBegin.ToShortDateString(), DateEnd.ToShortDateString(), SelectedDepartment.Id);

            Count_Visit = _Get_CountVisit.Count + _Return_CountVisit.Count;
        }

        #endregion

        public VisitPlaceViewModel(FirebirdHelper helper, DateTime dateBegin, DateTime dateEnd)
        {
            this.helper = helper;
            this.DateBegin = dateBegin;
            this.DateEnd = dateEnd;
        }

        public override void Dispose()
        {
            base.Dispose();
        }
    }
}
