﻿using Core_Library.Commands;
using Core_Library.Models;
using Core_Library.Service;
using Core_Library.ViewModels.Base;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace Core_Library.ViewModels
{
    internal class BoolPlaceViewModel : BaseViewModel
    {
        public BoolPlaceViewModel(FirebirdHelper helper, DateTime date_begin, DateTime date_end)
        {
            this.helper = helper;
            this._Date_begin = date_begin;
            this._Date_end = date_end;
        }

        private readonly FirebirdHelper helper;

        private ObservableCollection<Place> _Place;
        public ObservableCollection<Place> Place
        {
            get => _Place;
            set => Set(ref _Place, value);
        }

        private DateTime _Date_begin;

        public DateTime Date_begin
        {
            get => _Date_begin;
            set => Set(ref _Date_begin, value);
        }

        private DateTime _Date_end;

        public DateTime Date_end
        {
            get => _Date_end;
            set => Set(ref _Date_end, value);
        }


        private ICommand _GetBooks;
        public ICommand GetBooks => _GetBooks ??= new LambdaCommand(LoadedTable);


        private async void LoadedTable(object p)
        {
            try
            {
               Place = await helper.GetBookPlace(Date_begin.ToShortDateString(), Date_end.ToShortDateString());
            }
             catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }



        public override void Dispose()
        {
            base.Dispose();
        }

    }
}
