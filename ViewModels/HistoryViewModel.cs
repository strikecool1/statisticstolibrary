﻿using Core_Library.Commands;
using Core_Library.Models;
using Core_Library.Service;
using Core_Library.ViewModels.Base;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace Core_Library.ViewModels
{
    internal class HistoryViewModel : BaseViewModel
    {

        public HistoryViewModel(FirebirdHelper firebird)
        {
            this.firebird = firebird;
        }

        private readonly FirebirdHelper firebird;
        private string _Mounth;

        public string Mounth
        {
            get => _Mounth;
            set => Set(ref _Mounth, value);
        }

        private string _Year;

        public string Year
        {
            get => _Year;
            set => Set(ref _Year, value);
        }

        private ObservableCollection<Histrory> _History;
        public ObservableCollection<Histrory> History
        {
            get => _History;
            set => Set(ref _History, value);
        }


        private ICommand _LoadedData;
        public ICommand LoadedData => _LoadedData ??= new LambdaCommand(SelectData, CanLoadedCount);

        private bool CanLoadedCount(object arg) => !string.IsNullOrEmpty(Mounth) && !string.IsNullOrEmpty(Year);


        private async void SelectData(object p)
        {
            try
            {
                History = await firebird.GetHistory(Mounth, Year);
            }       
             catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

        }

        public override void Dispose()
        {
            base.Dispose();
        }
    }
}
