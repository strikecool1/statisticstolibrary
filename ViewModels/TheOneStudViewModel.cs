﻿using Core_Library.Commands;
using Core_Library.Models;
using Core_Library.Service;
using Core_Library.ViewModels.Base;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace Core_Library.ViewModels
{
    internal class TheOneStudViewModel : BaseViewModel
    {

        private DateTime _Date_begin;
        private DateTime _Date_end;
        private ObservableCollection<Departments> _Departments;
        private readonly FirebirdHelper helper;
        private TheOneSotr _TheOne;
        private Departments _SelectedDepartment;

        public TheOneStudViewModel(FirebirdHelper helper, DateTime dateBegin, DateTime dateEnd)
        {
            this.helper = helper;
            _Date_begin = dateBegin;
            _Date_end = dateEnd;
        }

        private int _SumTheOne = 0;
        public int SumTheOne { get => _SumTheOne; set => Set(ref _SumTheOne, value); }

        public TheOneSotr TheOne
        {
            get => _TheOne;
            set => Set(ref _TheOne, value);
        }

        public ObservableCollection<Departments> Departments
        {
            get => _Departments;
            set => Set(ref _Departments, value);
        }

        public Departments SelectedDepartment
        {
            get => _SelectedDepartment;
            set => Set(ref _SelectedDepartment, value);
        }

        public DateTime Date_begin
        {
            get => _Date_begin;
            set => Set(ref _Date_begin, value);
        }

        public DateTime Date_end
        {
            get => _Date_end;
            set => Set(ref _Date_end, value);
        }

        private ICommand _LoadedDep;

        public ICommand LoadedDep => _LoadedDep ??= new LambdaCommand(LoadedTable);


        private ICommand _LoadedData;
        public ICommand LoadedData => _LoadedData ??= new LambdaCommand(SelectedData, CanLoadedCount);

        // Вывести список только в том случае если выбран абонемент
        private bool CanLoadedCount(object parameter) => SelectedDepartment != null;

        private async void LoadedTable(object p)
        {
            Departments = await helper.GetDepartmentToStud();
        }

        private async void SelectedData(object p)
        {
            try
            {
                TheOne = await helper.GetTheOneStud(Date_begin.ToShortDateString(), Date_end.ToShortDateString(), SelectedDepartment.Id);
                if (TheOne.Sum == 0)
                    MessageBox.Show("За данный период ничего не найдено!");

                SumTheOne =
                   TheOne.Count_Aspirant +
                   TheOne.Count_Dovuz +
                   TheOne.Count_Magistatura +
                   TheOne.Count_Mladg_Special +
                   TheOne.Count_Prepod +
                   TheOne.Count_Sotr +
                   TheOne.Count_Stud_DO +
                   TheOne.Count_Stud_EK +
                   TheOne.Count_Stud_ZO;
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }
           
        }

        public override void Dispose()
        {
            base.Dispose();
        }

    }
}
