﻿using Core_Library.Commands;
using Core_Library.Models;
using Core_Library.Service;
using Core_Library.ViewModels.Base;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace Core_Library.ViewModels
{
    internal class TheOneViewModel : BaseViewModel
    {

        public TheOneViewModel(FirebirdHelper helper, DateTime dateBegin, DateTime dateEnd)
        {
            this.helper = helper;
            _Date_begin = dateBegin;
            _Date_end = dateEnd;
        }


        #region Переменные
        private readonly FirebirdHelper helper;
        private ObservableCollection<Departments> _Departments;
        private ICommand _LoadedDep;
        private DateTime _Date_begin;
        private DateTime _Date_end;
        private Departments _SelectedDepartment;

        private int _SumTheOne = 0;
        public int SumTheOne { get => _SumTheOne; set => Set(ref _SumTheOne, value); }

        public ObservableCollection<Departments> Departments
        {
            get => _Departments;
            set => Set(ref _Departments, value);
        }


        private TheOneSotr _TheOne;

        public TheOneSotr TheOne
        {
            get => _TheOne;
            set => Set(ref _TheOne, value);
        }

        public DateTime Date_begin
        {
            get => _Date_begin;
            set => Set(ref _Date_begin, value);
        }

        public DateTime Date_end
        {
            get => _Date_end;
            set => Set(ref _Date_end, value);
        }

       

        public Departments SelectedDepartment
        {
            get => _SelectedDepartment;
            set => Set(ref _SelectedDepartment, value);
        }


        public ICommand LoadedDep => _LoadedDep ??= new LambdaCommand(LoadedTable);

        private ICommand _LoadedOne;

        public ICommand LoadedOne => _LoadedOne ??= new LambdaCommand(SelectedData , CanLoadedCount);


        private bool CanLoadedCount(object parameter) => SelectedDepartment != null;

        #endregion       


        private async void LoadedTable(object p)
        {
            try
            {
                Departments = await helper.GetDepartmentToSotr();
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }
           
        }

        private async void SelectedData(object p)
        {
            try
            {
                TheOne = await helper.GetTheOneSotr(Date_begin.ToShortDateString(), Date_end.ToShortDateString(), 4);
                if(TheOne.Sum == 0)
                    MessageBox.Show("За данный период ничего не найдено!");

                // Итоговая сумма
                SumTheOne =
                   TheOne.Count_Aspirant +
                   TheOne.Count_Dovuz +
                   TheOne.Count_Magistatura +
                   TheOne.Count_Mladg_Special +
                   TheOne.Count_Prepod +
                   TheOne.Count_Sotr +
                   TheOne.Count_Stud_DO +
                   TheOne.Count_Stud_EK +
                   TheOne.Count_Stud_ZO;
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }
            
        }

        public override void Dispose()
        {
            base.Dispose();
        }

    }
}
