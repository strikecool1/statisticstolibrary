﻿using Core_Library.Service;
using Core_Library.ViewModels.Base;

namespace Core_Library.ViewModels
{
    internal class MainViewModel : BaseViewModel
    {

        private readonly NavigationStore _NavigationStore;

        public BaseViewModel CurrentViewModel => _NavigationStore.CurrentViewModel;

        public MainViewModel(NavigationStore navigationStore)
        {
            _NavigationStore = navigationStore;

            _NavigationStore.CurrentViewModelChanged += OnCurrentViewModelChanged;
        }

        private void OnCurrentViewModelChanged()
        {
            OnPropertyChanged(nameof(CurrentViewModel));
        }

        // Освобождаем ресурсы
        public override void Dispose()
        {
            base.Dispose();
        }


    }
}
