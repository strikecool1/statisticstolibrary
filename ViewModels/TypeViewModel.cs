﻿using Core_Library.Commands;
using Core_Library.Service;
using Core_Library.ViewModels.Base;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace Core_Library.ViewModels
{
    internal class TypeViewModel : BaseViewModel
    {
        public TypeViewModel(FirebirdHelper helper, DateTime date_begin, DateTime date_end)
        {
            this.helper = helper;
            this._Date_begin = date_begin;
            this._Date_end = date_end;
        }

        private readonly FirebirdHelper helper;

        private ObservableCollection<Models.Type> _Types;
        public ObservableCollection<Models.Type> Types
        {
            get => _Types;
            set => Set(ref _Types, value);
        }

        private DateTime _Date_begin;

        public DateTime Date_begin
        {
            get => _Date_begin;
            set => Set(ref _Date_begin, value);
        }

        private DateTime _Date_end;

        public DateTime Date_end
        {
            get => _Date_end;
            set => Set(ref _Date_end, value);
        }

        private ICommand _GetBooks;
        public ICommand GetBooks => _GetBooks ??= new LambdaCommand(LoadedTable);

        private async void LoadedTable(object p)
        {
            try
            {
                Types = await helper.GetBookType(Date_begin.ToShortDateString(), Date_end.ToShortDateString());
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

        }

        public override void Dispose()
        {
            base.Dispose();
        }

    }
}
