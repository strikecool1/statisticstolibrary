﻿using Core_Library.Service;
using Core_Library.ViewModels;
using System.Text;
using System.Windows;

namespace Core_Library
{
    public partial class App : Application
    {

        protected override void OnStartup(StartupEventArgs e)
        {
            // Регистрация кодировки win 1251
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Encoding.GetEncoding(1251);

            NavigationStore navigationStore = new NavigationStore();

            // Навигация запускает первой формой Авторизацию
            navigationStore.CurrentViewModel = new LoginViewModel(navigationStore);

            MainWindow = new MainWindow()
            {
                DataContext = new MainViewModel(navigationStore)
            };

            MainWindow.Show();

            base.OnStartup(e);
        }
    }
}
