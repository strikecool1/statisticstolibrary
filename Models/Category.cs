﻿namespace Core_Library.Models
{
    class Category
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Get_Count { get; set; }
    }
}
