﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core_Library.Models
{
    internal class VisitPerson
    {
        public string Place { get; set; }

        public string PersonName { get; set; }

    }
}
