﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core_Library.Models
{
    internal class Persons
    {
        public long Pin { get; set; } // Краткий id Персоны

        public string FullName { get; set; } // ФИО

        public string Book { get; set; } // Книга 

        public string Inventory { get; set; } // Интвентарный номер
        public string Author { get; set; }

        public string StyleBook { get; set; }

        public string DateGet { get; set; } // Дата выдачи

        public string DateRerutn { get; set; } // Дата возврата

        public string Place { get; set; }

        public string UserGet { get; set; } // Кем выдана книга

        public string CategoryPerson { get; set; } // Кем является человек: Студентом , Сотрдуником , Аспирантом и т.д.

        public string TypeBook { get; set; }

        public int Count_All { get; set; }

    }
}
