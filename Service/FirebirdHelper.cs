﻿using Core_Library.Models;
using FirebirdSql.Data.FirebirdClient;
using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Threading.Tasks;

namespace Core_Library.Service
{
    internal class FirebirdHelper
    {
        private readonly string URLDataBase = ConfigurationManager.AppSettings["connection"];

        // Выдать всю книговыдачю за перод
        public async Task<ObservableCollection<Persons>> GetBooks(string date_begin , string date_end)
        {
            ObservableCollection<Persons> list = new ObservableCollection<Persons>();

            using (FbConnection connection = new FbConnection(URLDataBase))
            {
                connection.Open();

                using var transaction = connection.BeginTransaction();

                string sql = $"select * from GET_BOOKS('{date_begin}','{date_end}')";

                using FbCommand command = new FbCommand(sql, connection, transaction);

                FbDataReader reader = await command.ExecuteReaderAsync();

                while (await reader.ReadAsync())
                {
                    list.Add(new Persons
                    {
                        FullName = reader.GetString(0),
                        CategoryPerson = reader.GetString(1),
                        Book = reader.GetString(2),
                        Author = reader.GetString(3),
                        StyleBook = reader.GetString(4),
                        TypeBook = reader.GetString(5),
                        Place = reader.GetString(9),
                        UserGet = reader.GetString(10),
                        DateGet = reader["date_get"] != DBNull.Value ? reader.GetDateTime(6).Date.ToShortDateString() : "Пусто",
                        DateRerutn = reader["date_return"] != DBNull.Value ? reader.GetDateTime(7).Date.ToShortDateString() : "Пусто",


                    });
                }
                reader.Close();
                connection.Close();
            }
            return list;
        }

        public async Task<TheOneSotr> GetTheOne(string date_begin, string date_end, int id_place)
        {
            TheOneSotr list = new TheOneSotr();

            using (FbConnection connection = new FbConnection(URLDataBase))
            {
                connection.Open();

                using var transaction = connection.BeginTransaction();

                string sql = $" select first 1  * from NEW_THE_ONE('{date_begin}','{date_end}' , {id_place})  order by count_other desc";

                using FbCommand command = new FbCommand(sql, connection, transaction);

                FbDataReader reader = await command.ExecuteReaderAsync();

                while (await reader.ReadAsync())
                {
                  //  list.Count_Other = reader.GetInt32(10);

                }
                reader.Close();
                connection.Close();
            }
            return list;
        }

        public async Task<TheOneSotr> GetTheOneSotr(string date_begin, string date_end , int id_place)
        {
            TheOneSotr list = new();

            using (FbConnection connection = new(URLDataBase))
            {
                connection.Open();

                using FbTransaction transaction = connection.BeginTransaction();

                string sql = $" select first 1  * from THE_ONE_SOTR('{date_begin}','{date_end}' , {id_place})  order by count_index desc";

                using FbCommand command = new(sql, connection, transaction);

                FbDataReader reader = await command.ExecuteReaderAsync();

                while (await reader.ReadAsync())
                {
                    list.Count_Aspirant = reader.GetInt32(2);
                    list.Count_Prepod = reader.GetInt32(3);
                    list.Count_Sotr = reader.GetInt32(4);
                    list.Count_Dovuz = reader.GetInt32(5);

                    list.Count_Magistatura = reader.GetInt32(6);
                    list.Count_Stud_DO = reader.GetInt32(7);
                    list.Count_Stud_ZO = reader.GetInt32(8);
                    list.Count_Stud_EK = reader.GetInt32(9);

                    list.Count_Mladg_Special = reader.GetInt32(10);

                    list.Sum = reader.GetInt32(12);

                }
                reader.Close();
                connection.Close();
            }
            return list;
        }

        public async Task<TheOneSotr> GetTheOneStud(string date_begin, string date_end , int idPlace)
        {
            TheOneSotr list = new();

            using (FbConnection connection = new(URLDataBase))
            {
                connection.Open();

                using FbTransaction transaction = connection.BeginTransaction();


                string sql = "";

                if (idPlace == 2)
                    sql = $" select first 1  * from THE_ONE_STUD_IN_DEP_2('{date_begin}','{date_end}' , {idPlace})  order by count_index desc";

                else 
                    sql = $" select first 1  * from THE_ONE_STUD_IN_DEP_3('{date_begin}','{date_end}' , {idPlace})  order by count_index desc";

                using FbCommand command = new FbCommand(sql, connection, transaction);

                FbDataReader reader = await command.ExecuteReaderAsync();

                while (await reader.ReadAsync())
                {
                    list.Count_Aspirant = reader.GetInt32(2);
                    list.Count_Prepod = reader.GetInt32(3);
                    list.Count_Sotr = reader.GetInt32(4);
                    list.Count_Dovuz = reader.GetInt32(5);

                    list.Count_Magistatura = reader.GetInt32(6);
                    list.Count_Stud_DO = reader.GetInt32(7);
                    list.Count_Stud_ZO = reader.GetInt32(8);
                    list.Count_Stud_EK = reader.GetInt32(9);

                    list.Count_Mladg_Special = reader.GetInt32(10);

                    list.Sum = reader.GetInt32(12);

                }
                reader.Close();
                connection.Close();
            }
            return list;
        }
        // Вывести книгавыдачу по категориям 
        public async Task<ObservableCollection<Category>> GetBookCategory(string Date_Begin, string Date_End)
        {
            ObservableCollection<Category> list = new ObservableCollection<Category>();

            using (FbConnection connection = new FbConnection(URLDataBase))
            {
                connection.Open();

                using FbTransaction transaction = connection.BeginTransaction();

                string sql =
                    " SELECT category.name, COUNT(card.item_id)" +
                    " FROM physical_person pers " +
                    " INNER JOIN person_card card on card.person_id = pers.id " +
                    " INNER JOIN storage_list place on card.deliver_kod = place.kod  " +
                    " inner join reader_category category on pers.category_id = category.id " +
                    " inner join doc_item item on card.item_id = item.item_id " +
                    " inner join document doc on item.doc_id = doc.doc_id " +
                    " inner join type_list typ on doc.type_kod = typ.kod " +
                    " inner join doctype doc_type on doc.doc_type = doc_type.code " +
                    $" WHERE (card.get_date >= '{Date_Begin}' AND card.get_date <= '{Date_End}') and place.stype = 'D' " +
                    " GROUP BY category.name ;";

                using FbCommand command = new FbCommand(sql, connection, transaction);

                FbDataReader reader = await command.ExecuteReaderAsync();

                while (await reader.ReadAsync())
                {
                    list.Add(new Category
                    {
                        Name = reader.GetString(0),
                        Get_Count = reader.GetInt32(1),
                    });
                }
                reader.Close();
                connection.Close();
            }
            return list;
        }
        // 
        public async Task<ObservableCollection<Histrory>> GetKOSTIL(string date_begin, string date_end)
        {
            ObservableCollection<Histrory> list = new();

            using (FbConnection connection = new FbConnection(URLDataBase))
            {
                connection.Open();

                using FbTransaction transaction = connection.BeginTransaction();

                string sql =
                    "   select sp.name, count (distinct pers.id)"
                    + " from physical_person pers"
                     + " inner join reader_category category on pers.category_id = category.id "
                     +  " inner join person_card card on card.person_id = pers.id "
                     +  "  inner join reader_srv_place rp on pers.id = rp.reader_id"
                     +  "  inner join service_place sp on sp.id = rp.place_id" 
                     +  $" where (card.get_date >= '{date_begin}' AND card.get_date <= '{date_end}') "
                     +  "   and sp.id <> 2 "
                     +  "   and sp.id <> 3 "
                     +  "   and sp.id <> 4 "
                     +  "   group by 1 ";
                using FbCommand command = new FbCommand(sql, connection, transaction);

                FbDataReader reader = await command.ExecuteReaderAsync();

                while (await reader.ReadAsync())
                {
                    list.Add(new Histrory
                    {
                        Place = reader.GetString(0),
                        Counts = reader.GetInt32(1),
                    });
                }
                reader.Close();
                connection.Close();
            }
            return list;
        }

        public async Task<ObservableCollection<Histrory>> GetHistory(string MON, string YEAR)
        {
            ObservableCollection<Histrory> list = new();

            using (FbConnection connection = new FbConnection(URLDataBase))
            {
                connection.Open();

                using FbTransaction transaction = connection.BeginTransaction();

                string sql =
                     "   select sp.name, count (distinct pers.id)"
                    + " from physical_person pers"
                     + " inner join reader_category category on pers.category_id = category.id "
                     + " inner join person_card card on card.person_id = pers.id "
                     + "  inner join reader_srv_place rp on pers.id = rp.reader_id "
                     + "  inner join service_place sp on sp.id = rp.place_id "
                     + $" where (extract(year from card.get_date)='{YEAR}') and (extract(month from card.get_date)='{MON}') "
                     + "   and sp.id <> 2 "
                     + "   and sp.id <> 3 "
                     + "   and sp.id <> 4 "
                     + "   group by 1 ";
                using FbCommand command = new FbCommand(sql, connection, transaction);

                FbDataReader reader = await command.ExecuteReaderAsync();

                while (await reader.ReadAsync())
                {
                    list.Add(new Histrory
                    {
                        Place = reader.GetString(0),
                        Counts = reader.GetInt32(1),
                    });
                }
                reader.Close();
                connection.Close();
            }
            return list;
        }

        // Вывести книгавыдачу по месту 
        public async Task<ObservableCollection<Place>> GetBookPlace(string Date_Begin, string Date_End)
        {
            ObservableCollection<Place> list = new ObservableCollection<Place>();

            using (FbConnection connection = new FbConnection(URLDataBase))
            {
                connection.Open();

                using FbTransaction transaction = connection.BeginTransaction();

                string sql =
                    " SELECT place.name, COUNT(card.item_id)" +
                    " FROM physical_person pers " +
                    " INNER JOIN person_card card on card.person_id = pers.id " +
                    " INNER JOIN storage_list place on card.deliver_kod = place.kod  " +
                    " inner join doc_item item on card.item_id = item.item_id " +
                    " inner join document doc on item.doc_id = doc.doc_id " +
                    " inner join type_list typ on doc.type_kod = typ.kod " +
                    " inner join doctype doc_type on doc.doc_type = doc_type.code " +
                    $" WHERE (card.get_date >= '{Date_Begin}' AND card.get_date <= '{Date_End}') and place.stype = 'D' " +
                    " GROUP BY place.name ;";

                using FbCommand command = new FbCommand(sql, connection, transaction);

                FbDataReader reader = await command.ExecuteReaderAsync();

                while (await reader.ReadAsync())
                {
                    list.Add(new Place
                    {
                        NamePlace = reader.GetString(0),
                        Get_Count = reader.GetInt32(1),
                    });
                }
                reader.Close();
                connection.Close();
            }
            return list;
        }

        // Вывести книгавыдачу по месту 
        public async Task<ObservableCollection<Models.Type>> GetBookType(string Date_Begin, string Date_End)
        {
            ObservableCollection<Models.Type> list = new ObservableCollection<Models.Type>();

            using (FbConnection connection = new FbConnection(URLDataBase))
            {
                connection.Open();

                using FbTransaction transaction = connection.BeginTransaction();

                string sql =
                    " SELECT type_list.name, COUNT(card.item_id)" +
                    " FROM physical_person pers " +
                    " INNER JOIN person_card card on card.person_id = pers.id " +
                    " INNER JOIN storage_list place on card.deliver_kod = place.kod  " +
                    " inner join doc_item item on card.item_id = item.item_id " +
                    " inner join document doc on item.doc_id = doc.doc_id " +
                    " inner join type_list typ on doc.type_kod = typ.kod " +
                    " inner join doctype doc_type on doc.doc_type = doc_type.code " +
                    $" WHERE (card.get_date >= '{Date_Begin}' AND card.get_date <= '{Date_End}') and place.stype = 'D' " +
                    " GROUP BY type_list.name ;";

                using FbCommand command = new FbCommand(sql, connection, transaction);

                FbDataReader reader = await command.ExecuteReaderAsync();

                while (await reader.ReadAsync())
                {
                    list.Add(new Models.Type
                    {
                        NameType = reader.GetString(0),
                        CountType = reader.GetInt32(1),
                    });
                }
                reader.Close();
                connection.Close();
            }
            return list;
        }


        // Вывести все места 
        public async Task<ObservableCollection<Place>> GetAllPlace()
        {
            ObservableCollection<Place> list = new ObservableCollection<Place>();

            using (FbConnection connection = new FbConnection(URLDataBase))
            {
                connection.Open();

                using var transaction = connection.BeginTransaction();

                string sql = " select st.kod, st.name from STORAGE_LIST st";

                using FbCommand command = new FbCommand(sql, connection, transaction);

                FbDataReader reader = await command.ExecuteReaderAsync();

                while (await reader.ReadAsync())
                {
                    list.Add(new Place
                    {
                       Id = reader.GetInt16(0),
                       NamePlace = reader.GetString(1),
                    });
                }
                reader.Close();
                connection.Close();
            }
            return list;
        }
        public async Task<ObservableCollection<Departments>> GetAllDepartments()
        {
            ObservableCollection<Departments> list = new ObservableCollection<Departments>();

            using (FbConnection connection = new FbConnection(URLDataBase))
            {
                connection.Open();

                using var transaction = connection.BeginTransaction();

                string sql = " select st.id, st.name from SERVICE_PLACE st";

                using FbCommand command = new FbCommand(sql, connection, transaction);

                FbDataReader reader = await command.ExecuteReaderAsync();

                while (await reader.ReadAsync())
                {
                    list.Add(new Departments
                    {
                        Id = reader.GetInt16(0),
                        Name = reader.GetString(1),
                    });
                }
                reader.Close();
                connection.Close();
            }
            return list;
        }

        public async Task<ObservableCollection<Departments>> GetDepartmentToSotr()
        {
            ObservableCollection<Departments> list = new ObservableCollection<Departments>();

            using (FbConnection connection = new FbConnection(URLDataBase))
            {
                connection.Open();

                using var transaction = connection.BeginTransaction();

                string sql = " select st.id, st.name from SERVICE_PLACE st where st.id = 4;";

                using FbCommand command = new FbCommand(sql, connection, transaction);

                FbDataReader reader = await command.ExecuteReaderAsync();

                while (await reader.ReadAsync())
                {
                    list.Add(new Departments
                    {
                        Id = reader.GetInt16(0),
                        Name = reader.GetString(1),
                    });
                }
                reader.Close();
                connection.Close();
            }
            return list;
        }

        public async Task<ObservableCollection<Departments>> GetDepartmentToStud()
        {
            ObservableCollection<Departments> list = new ObservableCollection<Departments>();

            using (FbConnection connection = new FbConnection(URLDataBase))
            {
                connection.Open();

                using var transaction = connection.BeginTransaction();

                string sql = " select st.id, st.name from SERVICE_PLACE st where st.id = 2 or st.id = 3;";

                using FbCommand command = new FbCommand(sql, connection, transaction);

                FbDataReader reader = await command.ExecuteReaderAsync();

                while (await reader.ReadAsync())
                {
                    list.Add(new Departments
                    {
                        Id = reader.GetInt16(0),
                        Name = reader.GetString(1),
                    });
                }
                reader.Close();
                connection.Close();
            }
            return list;
        }

        // Общая посещаемость (выдача)
        public async Task<ObservableCollection<VisitPerson>> GetVisit(string Date_Begin, string Date_End)
        {
            ObservableCollection<VisitPerson> list = new ObservableCollection<VisitPerson>();

            using (FbConnection connection = new FbConnection(URLDataBase))
            {
                connection.Open();

                using var transaction = connection.BeginTransaction();

                string sql = " select distinct serv.name , pers.name " +
                              " from physical_person pers " +
                              " inner join person_card card on card.person_id = pers.id " +
                              " inner join reader_category category on pers.category_id = category.id " +
                              " inner join reader_srv_place rp on pers.id = rp.reader_id " +
                              " inner join service_place serv on rp.place_id = serv.id " +
                              $" where card.get_date >= '{Date_Begin}' AND card.get_date <= '{Date_End}'; ";

                using FbCommand command = new FbCommand(sql, connection, transaction);

                FbDataReader reader = await command.ExecuteReaderAsync();

                while (await reader.ReadAsync())
                {
                    list.Add(new VisitPerson
                    {
                        Place = reader.GetString(0),
                        PersonName = reader.GetString(1),
                    });
                }
                reader.Close();
                connection.Close();
            }
            return list;
        }

        // Общая посещаемость возврат
        public async Task<ObservableCollection<VisitPerson>> ReturnVisit(string Date_Begin, string Date_End)
        {
            ObservableCollection<VisitPerson> list = new ObservableCollection<VisitPerson>();

            using (FbConnection connection = new FbConnection(URLDataBase))
            {
                connection.Open();

                using var transaction = connection.BeginTransaction();

                string sql = " select distinct serv.name , pers.name " +
                             " from physical_person pers " +
                             " inner join person_card card on card.person_id = pers.id " +
                             " inner join reader_category category on pers.category_id = category.id " +
                             " inner join reader_srv_place rp on pers.id = rp.reader_id " +
                             " inner join service_place serv on rp.place_id = serv.id " +
                             $" where card.return_date >= '{Date_Begin}' AND card.return_date <= '{Date_End}'; ";

                using FbCommand command = new FbCommand(sql, connection, transaction);

                FbDataReader reader = await command.ExecuteReaderAsync();

                while (await reader.ReadAsync())
                {
                    list.Add(new VisitPerson
                    {
                        Place = reader.GetString(0),
                        PersonName = reader.GetString(1),

                    });
                }
                reader.Close();
                connection.Close();
            }
            return list;
        }
        // Посещаемость по месту (Выдача)
        public async Task<ObservableCollection<VisitPerson>> VisitPlace_Get(string Date_Begin, string Date_End, int id_otdel)
        {
            // Place list = new Place();
            ObservableCollection<VisitPerson> list = new ObservableCollection<VisitPerson>();
            using FbConnection connection = new FbConnection(URLDataBase);
            connection.Open();

            using var transaction = connection.BeginTransaction();

            string sql = " select distinct serv.name , pers.name " +
                          " from physical_person pers " +
                          " inner join person_card card on card.person_id = pers.id " +
                          " inner join reader_category category on pers.category_id = category.id " +
                          " inner join reader_srv_place rp on pers.id = rp.reader_id " +
                          " inner join service_place serv on rp.place_id = serv.id " +
                          $" where card.get_date >= '{Date_Begin}' AND card.get_date <= '{Date_End}' and serv.id = {id_otdel}";

            using FbCommand command = new FbCommand(sql, connection, transaction);

            FbDataReader reader = await command.ExecuteReaderAsync();

            while (await reader.ReadAsync())
            {
                list.Add(new VisitPerson
                {
                    Place = reader.GetString(0),
                    PersonName = reader.GetString(1)
                });
            }
            reader.Close();
            connection.Close();

            return list;
        }

        // Посещаемость по месту (Возврат)
        public async Task<ObservableCollection<VisitPerson>> VisitPlace_Return(string Date_Begin, string Date_End, int id_otdel)
        {
            ObservableCollection<VisitPerson> list = new ObservableCollection<VisitPerson>();

            using (FbConnection connection = new FbConnection(URLDataBase))
            {
                connection.Open();

                using var transaction = connection.BeginTransaction();

                string sql = " select distinct serv.name , pers.name " +
                              " from physical_person pers " +
                              " inner join person_card card on card.person_id = pers.id " +
                              " inner join reader_category category on pers.category_id = category.id " +
                              " inner join reader_srv_place rp on pers.id = rp.reader_id " +
                              " inner join service_place serv on rp.place_id = serv.id " +
                              $" where card.return_date >= '{Date_Begin}' AND card.return_date <= '{Date_End}' and serv.id = {id_otdel}";

                using FbCommand command = new FbCommand(sql, connection, transaction);

                FbDataReader reader = await command.ExecuteReaderAsync();

                while (await reader.ReadAsync())
                {
                    list.Add(new VisitPerson
                    {
                        Place = reader.GetString(0),
                        PersonName = reader.GetString(1),
                    });
                }
                reader.Close();
                connection.Close();
            }
            return list;
        }

    }
}
