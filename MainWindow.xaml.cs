﻿using Core_Library.Themes.Extensions;
using System.Windows.Media;

namespace Core_Library
{
    public partial class MainWindow 
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            var accentBrush = TryFindResource("AccentColorBrush") as SolidColorBrush;
            if (accentBrush != null) accentBrush.Color.CreateAccentColors();
        }
    }
}
