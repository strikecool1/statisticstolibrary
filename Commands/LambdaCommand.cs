﻿using Core_Library.Commands.Base;
using System;

namespace Core_Library.Commands
{
    internal class LambdaCommand : BaseCommand
    {

        private readonly Action<object> _Execute;
        private readonly Func<object, bool> _CanExecute;

        public LambdaCommand(/*Func<Task> Execute*/ Action<object> Execute, Func<object, bool> CanExecute = null)
        {
            _Execute = Execute ?? throw new ArgumentNullException(nameof(Execute));
            _CanExecute = CanExecute;
        }

        protected override bool CanExecute(object p) => _CanExecute?.Invoke(p) ?? true;

        // protected override async Task Execute(object p) => await _Execute();

        protected override void Execute(object p) => _Execute(p);

    }
}
