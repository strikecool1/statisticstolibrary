﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace Core_Library.Commands.Base
{
    internal abstract class BaseCommand : ICommand
    {
        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }

        bool ICommand.CanExecute(object parameter) => CanExecute(parameter);

        void ICommand.Execute(object parameter)
        {
            if (((ICommand)this).CanExecute(parameter))
                Execute(parameter);
        }
        protected virtual bool CanExecute(object p) => true;

        // Если что для асинхронных методов
        protected abstract void /*Task*/ Execute(object p);
    }
}
